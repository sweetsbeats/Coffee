--how to structure a map.lua file

local tileString = [[
####
#  #
#  #
####
]]

local quadInfo = {
{ ' ', 0, 0 }, --grass
{ '#', 32, 0}  --box
}

local entityInfo = {}

local entityList = {}

newMap(32, 32, 'assets/tiles/countryside.png', tileString, quadInfo, entityInfo, entityList)
