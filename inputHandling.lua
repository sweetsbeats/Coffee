-- Sets current input mode
    --currentInputMode = {[10] = true}
-- Sets/Init time list for keys
    --dtList = {w = 0, a = 0, s = 0, d = 0}

--[[
function keyManagement(key, isPressed)
   if isPressed == true then
      dtList[key] = love.timer.getTime()
   end
   for x = 64, 1, -1 do
      if currentInputMode[x] == true then
	 inputMode[x](key, isPressed)
	 return
      end
   end
end
function inputModeWorld(key, isPressed)
   -- escape
   if key == "escape" and isPressed == false then
      love.event.quit()
      -- movement
   elseif key == "w" or key == "a" or key == "s" or key == "d" then
      if isPressed == true then
	 if dtList.w >= dtList.a and dtList.w >= dtList.s and dtList.w >= dtList.d then
	    moveDirection = "up"
	 elseif dtList.a >= dtList.s and dtList.a >= dtList.d then
	    moveDirection = "left"
	 elseif dtList.s >= dtList.d then
	    moveDirection = "down"
	 else
	    moveDirection = "right"
	 end
      elseif isPressed == false then
	 if key == "w" and moveDirection == "up" then
	    moveDirection = "stand"
	 elseif key == "a" and moveDirection == "left" then
	    moveDirection = "stand"
	 elseif key == "s" and moveDirection == "down" then
	    moveDirection = "stand"
	 elseif key == "d" and moveDirection == "right" then
	    moveDirection = "stand"
	 end
      end
   end
end
]]


function showJoysticks()
  if noController == false then
     for i, joystick in ipairs(joysticks) do
         love.graphics.print(joystick:getName(), 300, i * 60)
     end
   end
end


function checkForController()
  noController = true
  joysticks = love.joystick.getJoysticks()
  for c, controller in ipairs(joysticks) do
    controllerNum = 0
    controllerNum = controllerNum + 1
  end
  if controllerNum == 1 then
    love.joystick.loadGamepadMappings('controller.txt')
    for c, controller in ipairs(joysticks) do
      love.graphics.print(controller:getName(), 300, 300)
    end
  jStick = joysticks[1]
  noController = false
  end
end


moveDirection = nil
--[[ clockwise
    1 = up
    2 = right
    3 = down
    4 = left
]]

function playerMove(dt)
  if noController == true then
   moveKeyboard(player, dt)
  elseif noController == false then
    moveController(player, dt)
  end
end


function moveKeyboard(unit, dt)
  if love.keyboard.isDown('up') then
    unit.position.y = unit.position.y + dt * unit.speed
    moveDirection = 1
  else
    moveDirection = nil
  end
  if love.keyboard.isDown('right') then
    unit.position.x = unit.position.x + dt * unit.speed
    moveDirection = 2
  else
    moveDirection = nil
  end
  if love.keyboard.isDown('down') then
    unit.position.y = unit.position.y - dt * unit.speed
    moveDirection = 3
  else
    moveDirection = nil
  end
  if love.keyboard.isDown('left') then
    unit.position.x = unit.position.x - dt * unit.speed
    moveDirection = 4
  else
    moveDirection = nil
  end
end
 --includes keyboard controls
function moveController(unit, dt)
  if love.keyboard.isDown('up') or jStick:isGamepadDown('dpup') then
    unit.position.y = unit.position.y + dt * unit.speed
    moveDirection = 1
  else
    moveDirection = nil
  end
  if love.keyboard.isDown('right') or jStick:isGamepadDown('dpright') then
    unit.position.x = unit.position.x + dt * unit.speed
    moveDirection = 2
  else
    moveDirection = nil
  end
  if love.keyboard.isDown('down') or jStick:isGamepadDown('dpdown') then
    unit.position.y = unit.position.y - dt * unit.speed
    moveDirection = 3
  else
    moveDirection = nil
  end
  if love.keyboard.isDown('left') or jStick:isGamepadDown('dpleft') then
    unit.position.x = unit.position.x - dt * unit.speed
    moveDirection = 4
  else
    moveDirection = nil
  end
end
