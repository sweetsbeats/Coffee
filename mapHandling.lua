--local variables that take on the data from the map loaded
local tileW, tileH, tileset, Quads, tileTable, entityInfo, entities

function loadMap(path)
  love.filesystem.load(path)()
end



function newMap(tileWidth, tileHeight, tilesetPath, tileString, quadInfo, entityInfo, entityList)
--local variables take on the map data
tileW = tileWidth
tileH = tileHeight
tileset = love.graphics.newImage(tilesetPath)
entities = entityList
entInfo = entityInfo

local tilesetW, tilesetH = tileset:getWidth(), tileset:getHeight()

Quads = {}

--[[creates the character in the string inside of the Quads table, uses info[2] and info[3] to define
the size of the quad]]
for _,info in ipairs(quadInfo) do
  Quads[info[1]] = love.graphics.newQuad(info[2], info[3], tileW, tileH, tilesetW, tilesetH)
end
for _,info in ipairs(entInfo) do
  Quads[info[1]] = love.graphics.newQuad(info[2], info[3], tileW, tileH, tilesetW, tilesetH)
end


tileTable = {}

local width = #(tileString:match("[^\n]+"))

for x = 1, width, 1 do tileTable[x] = {} end



local rowIndex, columnIndex = 1, 1
for row in tileString:gmatch("[^\n]+") do
    assert(#row == width, 'Map is not aligned: width of row ' .. tostring(rowIndex) .. ' should be ' .. tostring(width) .. ', but it is ' .. tostring(#row))
    columnIndex = 1
    for character in row:gmatch(".") do
      tileTable[columnIndex][rowIndex] = character
      columnIndex = columnIndex + 1
    end
	rowIndex = rowIndex + 1
end
end

--useless function
--[[
function map2word(mx, my)
  return (mx-1)*tileW, (my-1)*tileH
end
]]
function drawMap()

  for columnIndex, column in ipairs(tileTable) do
    for rowIndex, char in ipairs(column) do
      local x, y = (columnIndex-1)*tileH, (rowIndex-1)*tileH
      love.graphics.draw(tileset, Quads[ char ], x, y)
    end
  end

--draw entities
for i, entity in ipairs(entities) do
  local name, x, y = entity[1], map2world(entity[2], entity[3])
  love.graphics.draw(tileset, Quads[name], x, y)
  end
end
