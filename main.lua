-- includes
require("inputHandling")

require 'mapHandling'

player = {
  position = {x = 0, y = 0},
  speed = 1
}
--checks for controller
checkForController()


function love.load()

loadMap('maps/testmap.lua')



end

function love.update(dt)
  --[[if not (currentMapPath == nextMapPath) then
   --Checks if the map to be loaded exists
   local exists = love.filesystem.exists(nextMapPath)
   --Create a map if the map does'nt exist
   if exists == false then
   generateMap(nextMapPath)
 end
   --Loads the map
   mapData = loadMap(nextMapPath)
   --Set the map to be loaded as current map
   currentMapPath = nextMapPath
end ]]

--dynamically chooses movement function depending on if controller is plugged in
playerMove(dt)

end

function love.draw()
   -- DEBUG INFO
   --drawMap(mapData, blockData)
   love.graphics.print(player.position.x, 400, 0)
   love.graphics.print(player.position.y, 0, 300)

  drawMap()


end

--
-- Input handling
--
--[[
function love.keypressed(key)
   keyManagement(key, true)
end

function love.keyreleased(key)
   keyManagement(key, false)
end
]]

--function drawMap(mapData, blockData)
--   local windowWidth, windowHeight, windowFlags = love.window.getMode()
--   local positionXResolution = windowWidth / numberOfTiles.x
--   local positionYResolution = windowHeight / numberOfTiles.y
--   for y = 1, mapData.y do
--      for x = 1, mapData.x do
--	 for z = 1, #mapData[x][y] do
--    local imageWidth, imageHeight = blockData[mapData[x][y][z]]["image"]:getDimensions()
--	   love.graphics.draw(blockData[mapData[x][y][z]]["image"], (x - 1) * positionXResolution, (y - 1) * positionYResolution, 0 , windowWidth / (imageWidth * numberOfTiles.x), windowHeight / (imageHeight * numberOfTiles.y))
--	 end
--      end
--   end
--end


--function mapInit()

     -- We should move it to somewhere else later
     -- which id gets which image and if it's solid (passable) or not
--     blockData = {[1] = {["image"] = love.graphics.newImage("resource/image/sand.png"), ["solid"] = false}}
--     blockData[2] = {["image"] = love.graphics.newImage("resource/image/blacksand.png"), ["solid"] = true}
     --Sets number of tiles to Display
--     numberOfTiles = {["x"] = 20, ["y"] = 11.25}
     --Sets current/next map path
--     currentMapPath, nextMapPath = 0, "world.map"
--end
